$(function () {
    $(document).scroll(function(){
        var $nav = $("#main-nav");
        var $dropdownNav = $("#dropdownMenu");
        $dropdownNav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
        $nav.toggleClass("scrolled", $(this).scrollTop() > $nav.height());
    });
});